package de.tadris.flang.desktop

import java.text.SimpleDateFormat
import java.util.*

object Log {
    private const val DEBUG = 0
    private const val INFO = 1
    private const val WARNING = 2
    private const val ERROR = 3
    private const val MIN_LOGLEVEL = DEBUG

    var loggerImpl: CustomLogger = SystemOutLogger()

    fun d(tag: String, msg: String) {
        loggerImpl.d(tag, msg)
    }

    fun i(tag: String, msg: String) {
        loggerImpl.i(tag, msg)
    }

    fun w(tag: String, msg: String) {
        loggerImpl.w(tag, msg)
    }

    fun e(tag: String, msg: String) {
        loggerImpl.e(tag, msg)
    }

    class SystemOutLogger : CustomLogger {

        override fun e(tag: String, msg: String) {
            log(tag, msg, ERROR)
        }

        override fun w(tag: String, msg: String) {
            log(tag, msg, WARNING)
        }

        override fun i(tag: String, msg: String) {
            log(tag, msg, INFO)
        }

        override fun d(tag: String, msg: String) {
            log(tag, msg, DEBUG)
        }

        private fun log(first: String, msg: String, level: Int) {
            if (level < MIN_LOGLEVEL) {
                return
            }
            val loglevel = when (level) {
                DEBUG -> "[ DEB]"
                WARNING -> "[WARN]"
                ERROR -> "[ ERR]"
                else -> "[INFO]"
            }
            output("[$date][$loglevel][$first]: $msg")
        }

        private fun output(msg: String?) {
            println(msg)
        }

        private val date: String
            get() = SimpleDateFormat("HH:mm:ss").format(Date())

    }

    interface CustomLogger {

        fun d(tag: String, msg: String)
        fun i(tag: String, msg: String)
        fun w(tag: String, msg: String)
        fun e(tag: String, msg: String)

    }
}