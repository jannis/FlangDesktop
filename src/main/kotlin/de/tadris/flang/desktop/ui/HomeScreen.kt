package de.tadris.flang.desktop.ui

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import de.tadris.flang.network_api.model.GameConfiguration
import de.tadris.flang.network_api.util.DefaultConfigurations

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun HomeScreen(){
    Column(modifier = Modifier.fillMaxSize()) {
        LazyVerticalGrid(
            columns = GridCells.Adaptive(100.dp),
            horizontalArrangement = Arrangement.Center,
            modifier = Modifier.width(305.dp).align(Alignment.CenterHorizontally),
        ){
            items(items = DefaultConfigurations.getConfigurations()){ item ->
                ConfigurationView(item.first, item.second)
            }
        }
    }
}

@Composable
fun ConfigurationView(title: String, configuration: GameConfiguration){
    Card(Modifier.padding(5.dp)) {
        Box(Modifier.clickable {
            // configuration was clicked
            // TODO
        }.fillMaxSize()) {
            Column(Modifier.padding(top = 15.dp, bottom = 15.dp).fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally) {
                if(!configuration.isCustomGame()){
                    Text(if(configuration.infiniteTime) SpecialChars.INFINITE else {
                        (configuration.time / 1000 / 60).toInt().toString() + " min"
                    }, style = MaterialTheme.typography.subtitle1)
                    Text(title, style = MaterialTheme.typography.subtitle2)
                }else{ // custom game
                    Text(title, style = MaterialTheme.typography.subtitle1)
                }
            }
        }
    }
}