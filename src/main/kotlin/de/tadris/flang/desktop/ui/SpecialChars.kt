package de.tadris.flang.desktop.ui

object SpecialChars {

    const val INFINITE = "∞"
    const val SEPARATION_POINT = " • "

}