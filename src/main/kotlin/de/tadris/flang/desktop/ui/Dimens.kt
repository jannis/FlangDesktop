package de.tadris.flang.desktop.ui

import androidx.compose.ui.unit.dp

object Dimens {

    val paddingSmall = 6.dp
    val paddingMedium = 12.dp

    val cornerSmall = 3.dp
    val cornerMedium = 6.dp
    val cornerBig = 12.dp

    val sideBar = 300.dp

}