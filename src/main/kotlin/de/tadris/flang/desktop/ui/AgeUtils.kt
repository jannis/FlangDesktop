package de.tadris.flang.desktop.ui

import java.util.concurrent.TimeUnit

object AgeUtils {

    private val YEAR = TimeUnit.DAYS.toMillis(365)
    private val MONTH = TimeUnit.DAYS.toMillis(30)
    private val WEEK = TimeUnit.DAYS.toMillis(7)
    private val DAY = TimeUnit.DAYS.toMillis(1)
    private val HOUR = TimeUnit.HOURS.toMillis(1)
    private val MINUTE = TimeUnit.MINUTES.toMillis(1)

    fun getAgeString(age: Long): String {
        return when {
            age > YEAR -> {
                val years = (age / YEAR).toInt()
                string(if(years == 1) "time.ago.year.one" else "time.ago.year.other").format(years)
            }
            age > MONTH -> {
                val months = (age / MONTH).toInt()
                string(if(months == 1) "time.ago.month.one" else "time.ago.month.other").format(months)
            }
            age > WEEK -> {
                val weeks = (age / WEEK).toInt()
                string(if(weeks == 1) "time.ago.week.one" else "time.ago.week.other").format(weeks)
            }
            age > DAY -> {
                val days = (age / DAY).toInt()
                string(if(days == 1) "time.ago.day.one" else "time.ago.day.other").format(days)
            }
            age > HOUR -> {
                val hours = (age / HOUR).toInt()
                string(if(hours == 1) "time.ago.hour.one" else "time.ago.hour.other").format(hours)
            }
            age > MINUTE -> {
                val minutes = (age / MINUTE).toInt()
                string(if(minutes == 1) "time.ago.minute.one" else "time.ago.minute.other").format(minutes)
            }
            else -> {
                string("time.ago.moments")
            }
        }
    }

}