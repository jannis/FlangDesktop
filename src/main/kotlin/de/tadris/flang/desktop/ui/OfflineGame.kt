package de.tadris.flang.desktop.ui

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import de.tadris.flang.desktop.Log
import de.tadris.flang.network_api.model.GameConfiguration
import de.tadris.flang.network_api.model.GameInfo
import de.tadris.flang.network_api.model.GamePlayerInfo
import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.bot.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.concurrent.Executors
import kotlin.math.roundToInt

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun OfflineGame(openScreen: (screen: Screen) -> Unit){
    val gameState = rememberSaveable { generateGameState() }
    var board by rememberSaveable { mutableStateOf(Board()) }
    var computerConfiguration by rememberSaveable { mutableStateOf(ComputerConfiguration(5, ComputerStyle.CLASSIC)) }

    GameScreen(board, gameState.info, System.currentTimeMillis(), { newBoard, move ->
        board = newBoard.clone(true).executeOnBoard(move)
    }, gameState.userColor, creative = true, rightSide = {
        rightSideMenu(computerConfiguration){ computerConfiguration = it }
    }, openScreen = openScreen)

    LaunchedEffect(board){
        if(board.atMove == gameState.userColor.getOpponent() && !board.gameIsComplete()){
            val botMove = findBestMove(computerConfiguration, board)
            Log.d("OfflineGame", "-> ${botMove.count} evals")
            println(botMove.evaluations)
            board = board.clone(true).executeOnBoard(botMove.bestMove.move)
        }
    }
}

@Composable
private fun rightSideMenu(configuration: ComputerConfiguration, onUpdate: (ComputerConfiguration) -> Unit){
    Card(shape = RoundedCornerShape(Dimens.cornerMedium)) {
        Column(modifier = Modifier.padding(Dimens.paddingMedium)) {
            Text(string("game.offline.computer.style"), style = MaterialTheme.typography.h4)
            ComputerStyle.values().forEach { style ->
                Row(verticalAlignment = Alignment.CenterVertically) {
                    RadioButton(selected = configuration.style == style, {
                        onUpdate(configuration.copy(style = style))
                    })
                    Spacer(modifier = Modifier.size(Dimens.paddingMedium))
                    Text(style.label)
                }
            }
            Text(string("game.offline.computer.strength"), style = MaterialTheme.typography.h4)
            val strengthOptions = listOf(1, 2, 3, 5, 7)
            var sliderState by remember { mutableStateOf(strengthOptions.indexOf(configuration.strength).toFloat()) }
            Slider(sliderState, onValueChange = {
                sliderState = it
                onUpdate(configuration.copy(strength = strengthOptions[it.roundToInt()]))
            }, valueRange = 0f..strengthOptions.lastIndex.toFloat(), steps = strengthOptions.size - 2)
            Text(
                text = string("game.offline.computer.strength") + ": " + configuration.strength,
                modifier = Modifier.align(Alignment.CenterHorizontally),
            )
        }
    }
}

private fun generateGameState(): OfflineGameState {
    val playerInfo = GamePlayerInfo("Player 1", 0f, -1L, 0f, false, "")
    val botInfo = GamePlayerInfo("Flangbot", 0f, -1L, 0f, true, "")

    val userColor = if(Math.random() > 0.5) Color.WHITE else Color.BLACK

    val whiteInfo = if(userColor == Color.WHITE) playerInfo else botInfo
    val blackInfo = if(userColor == Color.WHITE) botInfo else playerInfo

    val config = GameConfiguration(isRated = false, infiniteTime = true, time = -1L)
    val gameInfo = GameInfo(1, whiteInfo, blackInfo, "", 0, true, config, 0L, 0, 0)

    return OfflineGameState(gameInfo, userColor)
}

private class OfflineGameState(val info: GameInfo, val userColor: Color)

private data class ComputerConfiguration(val strength: Int, val style: ComputerStyle)

private enum class ComputerStyle(val label: String, val creator: () -> BoardEvaluation) {
    CLASSIC("Classic", { NeoBoardEvaluation() }),
    BRO_BOT("Bro Bot", { BlankBoardEvaluation() }),
    STAGE_BOT("Stage Bot", { StageEvaluation() }),
    HEATMAP("Heat Bot", { HeatmapEvaluation() }),
}

private suspend fun findBestMove(configuration: ComputerConfiguration, board: Board) = withContext(Dispatchers.IO){
    val cores = Runtime.getRuntime().availableProcessors()
    val bot = ParallelFlangBot(configuration.strength, configuration.strength, configuration.style.creator, threads = cores)
    bot.findBestMove(board.clone(true))
}