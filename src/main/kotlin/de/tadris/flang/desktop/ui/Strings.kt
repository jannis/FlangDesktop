package de.tadris.flang.desktop.ui

import java.util.ResourceBundle

private val bundle by lazy { ResourceBundle.getBundle("i18n/strings") }

fun string(key: String): String {
    return if (bundle.containsKey(key)) bundle.getString(key) else key
}