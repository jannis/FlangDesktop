package de.tadris.flang.desktop.ui

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.Icon
import androidx.compose.material.NavigationRail
import androidx.compose.material.NavigationRailItem
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp

@Composable
fun NavRail(currentScreen: Screen, onNavigate: (Screen) -> Unit){
    class NavigationElement(val configuration: Screen, val label: String, val icon: String)
    val navigationElements = listOf(
        NavigationElement(Screen.HomeScreen, string("menu.home"), Images.whiteFlanger),
        NavigationElement(Screen.FlangTv, string("menu.tv"), Images.tv),
        NavigationElement(Screen.OfflineGame, string("menu.offline"), Images.phone),
        NavigationElement(Screen.AnalysisGame(), string("menu.analysis"), Images.analysis),
    )

    NavigationRail {
        navigationElements.forEach { element ->
            NavigationRailItem(
                selected = currentScreen.javaClass == element.configuration.javaClass,
                onClick = { onNavigate(element.configuration) },
                icon = { Icon(painterResource(element.icon), contentDescription = element.label, modifier = Modifier.size(24.dp)) },
            )
        }
    }
}