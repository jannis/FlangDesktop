package de.tadris.flang.desktop.ui

import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.*
import de.tadris.flang.desktop.Log
import de.tadris.flang.desktop.data.DataProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

@Composable
fun TvGame(openScreen: (screen: Screen) -> Unit){
    var onlineGameId by remember { mutableStateOf(-1L) }

    LaunchedEffect(true){
        while (true){
            try{
                onlineGameId = fetchTvInfo().gameId
                Log.d("FlangTV", "GameID: $onlineGameId")
            }catch (e: Exception){
                e.printStackTrace()
            }
            delay(5000)
        }
    }

    OnlineGame(openScreen, onlineGameId)
}

private suspend fun fetchTvInfo() = withContext(Dispatchers.IO){ DataProvider.accessOpenAPI().tv() }