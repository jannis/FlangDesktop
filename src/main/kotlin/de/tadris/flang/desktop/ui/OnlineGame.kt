package de.tadris.flang.desktop.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontStyle
import de.tadris.flang.desktop.Log
import de.tadris.flang.desktop.data.DataProvider
import de.tadris.flang.network_api.model.GameConfiguration
import de.tadris.flang.network_api.model.GameInfo
import de.tadris.flang.network_api.model.GamePlayerInfo
import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun OnlineGame(openScreen: (screen: Screen) -> Unit, gameId: Long){
    var gameInfo by remember { mutableStateOf(generateDummyInfo()) }
    var board by remember { mutableStateOf(Board()) }
    var moves by remember { mutableStateOf(0) }
    var receivedTime by remember { mutableStateOf(System.currentTimeMillis()) }

    LaunchedEffect(gameId){
        if(gameId <= 0) return@LaunchedEffect // this is not a real game
        moves = 0
        do {
            try {
                gameInfo = fetchGameInfo(gameId, moves)
                receivedTime = System.currentTimeMillis()
                board = gameInfo.toBoard()
                if (gameInfo.moves >= moves) {
                    board.moveList.actions.subList(moves, board.moveList.size).forEach { action ->
                        // TODO handle new actions (show something, play sound, etc.)
                    }
                    moves = gameInfo.moves
                } else {
                    // we know more moves than the server, so we give him a bit time to keep up
                    delay(200)
                }
                Log.d("OnlineGame", "Received new game info, moves = $moves")
            }catch (e: Exception){
                e.printStackTrace()
                // TODO show error
                delay(5000)
            }
        } while (gameInfo.running)
    }

    GameScreen(board, gameInfo, receivedTime, { newBoard, move ->
        board = newBoard.clone(true).executeOnBoard(move)
    }, null, creative = false, leftSide = {
        LeftSideView(board, gameInfo)
    }, rightSide = { isFlipped ->
        RightSideView(board, gameInfo, receivedTime, isFlipped)
    }, openScreen = openScreen)
}

@Composable
private fun LeftSideView(gameBoard: Board, gameInfo: GameInfo){
    Card(shape = RoundedCornerShape(Dimens.cornerMedium), modifier = Modifier.padding(Dimens.paddingMedium)) {
        Column {
            Column(modifier = Modifier.padding(Dimens.paddingMedium)) {
                Text(
                    text = getGameInfoDescription(gameInfo),
                    style = MaterialTheme.typography.body1,
                )
                Text(
                    text = AgeUtils.getAgeString(System.currentTimeMillis() - gameInfo.lastAction),
                    style = MaterialTheme.typography.body2,
                )
            }
            if(!gameInfo.running || gameBoard.gameIsComplete()){
                Divider()
                Text(
                    text = getGameStateDescription(gameInfo, gameBoard),
                    fontStyle = FontStyle.Italic,
                    modifier = Modifier
                        .padding(Dimens.paddingMedium)
                        .align(Alignment.CenterHorizontally),
                    style = MaterialTheme.typography.body1,
                )
            }
        }
    }
}

@Composable
fun RightSideView(gameBoard: Board, gameInfo: GameInfo, receivedTime: Long, isFlipped: Boolean){
    val upperColor = if(isFlipped) Color.WHITE else Color.BLACK
    val lowerColor = if(isFlipped) Color.BLACK else Color.WHITE

    ClockView(gameBoard, gameInfo, receivedTime, upperColor)

    Card(shape = RoundedCornerShape(Dimens.cornerMedium), modifier = Modifier.padding(top = Dimens.paddingMedium, bottom = Dimens.paddingMedium)) {
        Column {
            GamePlayerInfoView(gameInfo.getPlayer(upperColor))
            GamePlayerInfoView(gameInfo.getPlayer(lowerColor))
        }
    }

    ClockView(gameBoard, gameInfo, receivedTime, lowerColor)
}

private fun generateDummyInfo(): GameInfo {
    val white = GamePlayerInfo("Loading...", 0f, -1L, 0f, false, "")
    val black = GamePlayerInfo("Loading...", 0f, -1L, 0f, false, "")

    val config = GameConfiguration(isRated = false, infiniteTime = true, time = -1L)
    return GameInfo(1, white, black, "", 0, true, config, 0L, 0, 0)
}

private suspend fun fetchGameInfo(gameId: Long, moves: Int) =
    withContext(Dispatchers.IO){ DataProvider.accessOpenAPI().getGameInfo(gameId, moves) }