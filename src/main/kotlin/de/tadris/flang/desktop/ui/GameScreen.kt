package de.tadris.flang.desktop.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.focusable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.input.key.*
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import de.tadris.flang.network_api.model.GameInfo
import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.TimeUtils
import de.tadris.flang_lib.action.ActionList
import de.tadris.flang_lib.action.Move
import kotlinx.coroutines.delay

@ExperimentalComposeUiApi
@Composable
fun GameScreen(
    gameBoard: Board,
    gameInfo: GameInfo,
    receivedTime: Long,
    onMoveRequested: (Board, Move) -> Unit,
    myColor: Color?,
    creative: Boolean,
    openScreen: (screen: Screen) -> Unit,
    leftSide: @Composable () -> Unit = { },
    rightSide: @Composable (isFlipped: Boolean) -> Unit = { },
    isFlippedAtStart: Boolean = myColor == Color.BLACK,
) {
    var displayedBoard by remember { mutableStateOf(gameBoard.clone(true)) }
    var isFlipped by remember { mutableStateOf(isFlippedAtStart) }
    val focusRequester = remember { FocusRequester() }

    fun displayedBoardIsGameBoard() = gameBoard.moveNumber == displayedBoard.moveNumber

    fun back(){
        val moveList = displayedBoard.clone(true).moveList
        if(moveList.actions.isNotEmpty()){
            println("undoing: " + moveList.rewind())
            displayedBoard = moveList.board
            println("game: ${gameBoard.getFMN2()}")
            println("displayed: ${displayedBoard.getFMN2()}")
        }
    }

    fun next(){
        if(displayedBoardIsGameBoard()) return
        val index = displayedBoard.moveList.size
        val action = gameBoard.moveList[index]
        println("redoing: $action")
        val newBoard = displayedBoard.clone(true)
        action.applyToBoard(newBoard)
        displayedBoard = newBoard
    }

    fun skipToStart(){
        displayedBoard = gameBoard.moveList.base?.clone(true) ?: Board()
    }

    fun skipToEnd(){
        displayedBoard = gameBoard.clone(true)
    }

    Column(modifier = Modifier.onKeyEvent {
        if(it.type != KeyEventType.KeyDown) return@onKeyEvent false
        when(it.key){
            Key.DirectionRight -> { next(); true }
            Key.DirectionLeft -> { back(); true }
            Key.DirectionUp -> { skipToStart(); true }
            Key.DirectionDown -> { skipToEnd(); true }
            else -> false
        }
    }
        .focusable()
        .focusRequester(focusRequester),
    ) {
        Box(modifier = Modifier
            .fillMaxSize()
            .weight(1f),
            contentAlignment = Alignment.Center,
        ) {
            Row {
                Column(modifier = Modifier.fillMaxHeight().weight(1f).width(Dimens.sideBar), verticalArrangement = Arrangement.Center) {
                    leftSide()
                }
                Box(contentAlignment = Alignment.Center, modifier = Modifier.padding(20.dp).clip(RoundedCornerShape(Dimens.cornerBig))) {
                    BoardView(displayedBoard, BoardConfiguration(
                        isClickable = true,
                        animate = true,
                        isFlipped = isFlipped,
                        myColor = myColor,
                        moveIsAllowed = (myColor != null && displayedBoardIsGameBoard()) || creative,
                    )){ move ->
                        if(displayedBoardIsGameBoard()){
                            onMoveRequested(gameBoard, move)
                        }else if(creative){ // player has rewound the board and replayed a different path
                            onMoveRequested(displayedBoard, move)
                        }
                    }
                }
                Column(modifier = Modifier.fillMaxHeight().weight(1f).width(Dimens.sideBar), verticalArrangement = Arrangement.Center) {
                    rightSide(isFlipped)
                    GameActionList(gameInfo, gameBoard, displayedBoard){  moveIndex ->
                        displayedBoard = ActionList(gameBoard.clone(true).moveList.actions.subList(0, moveIndex).toMutableList()).board
                    }
                }
            }
        }
        BottomAppBar {
            Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
                GameActionButton(Images.swap, string("game.action.swap")){
                    isFlipped = !isFlipped
                }
                GameActionButton(Images.left, string("game.action.back"), enabled = displayedBoard.moveNumber > 0){
                    back()
                }
                GameActionButton(Images.right, string("game.action.next"), enabled = !displayedBoardIsGameBoard()){
                    next()
                }
                GameActionButton(Images.analysis, string("game.action.analysis")){
                    openScreen(Screen.AnalysisGame(displayedBoard.getFMN2(), isFlipped))
                }
            }
        }
    }

    LaunchedEffect(gameBoard){
        displayedBoard = gameBoard.clone(true)
        focusRequester.requestFocus()
    }

}

@Composable
fun GameActionList(info: GameInfo, gameBoard: Board, displayedBoard: Board, onSelectMove: (moveIndex: Int) -> Unit){
    val selectedIndex = displayedBoard.moveNumber - 1
    val actions = gameBoard.moveList.actions
    val lineCount = actions.size / 2 + (actions.size % 2)

    Card(shape = RoundedCornerShape(Dimens.cornerMedium)) {
        Column {
            for(lineIndex in 0 until lineCount){
                MoveLine(gameBoard, lineIndex, selectedIndex, onSelectMove)
            }
            if(!info.running || gameBoard.gameIsComplete()){
                Divider()
                Text(
                    text = getGameStateDescription(info, gameBoard),
                    fontStyle = FontStyle.Italic,
                    modifier = Modifier
                        .padding(Dimens.paddingMedium)
                        .align(Alignment.CenterHorizontally),
                )
            }
        }
    }
}

@Composable
private fun MoveLine(board: Board, lineIndex: Int, selectedIndex: Int, onSelectMove: (moveIndex: Int) -> Unit){
    Row {
        Text((lineIndex + 1).toString(), modifier = Modifier.weight(2f).padding(Dimens.paddingSmall), textAlign = TextAlign.Center)
        MoveCell(board, lineIndex * 2 + 0, selectedIndex, modifier = Modifier.weight(8f), onSelectMove)
        MoveCell(board, lineIndex * 2 + 1, selectedIndex, modifier = Modifier.weight(8f), onSelectMove)
    }
}

@Composable
private fun MoveCell(board: Board, index: Int, selectedIndex: Int, modifier: Modifier, onSelectMove: (moveIndex: Int) -> Unit){
    val move = board.moveList.actions.getOrNull(index)
    val selected = index == selectedIndex
    Row(modifier = modifier.background(
        if(selected) Colors.selectedMove else androidx.compose.ui.graphics.Color.Transparent
    ).clickable {
        if(move != null) onSelectMove(index + 1)
    }.padding(Dimens.paddingSmall), verticalAlignment = Alignment.CenterVertically) {
        Box(modifier = Modifier.size(24.dp)) {
            if(move is Move){
                PieceImage(move.piece, false)
            }
        }
        val boardAtThisMove = ActionList(board.clone(true).moveList.actions.subList(0, index).toMutableList()).board
        Text(move?.getShortNotation(boardAtThisMove) ?: "")
    }
}

@Composable
private fun GameActionButton(icon: String, label: String, enabled: Boolean = true, onClick: () -> Unit){
    IconButton(onClick, enabled = enabled){
        Icon(painterResource(icon), contentDescription = label)
    }
}

@Composable
fun ClockView(gameBoard: Board, gameInfo: GameInfo, receivedTime: Long, color: Color){
    val active = gameBoard.atMove == color && !gameBoard.gameIsComplete() && gameInfo.running
    var timeLeft by remember { mutableStateOf(gameInfo.getPlayer(color).time) }

    LaunchedEffect(gameInfo){
        timeLeft = gameInfo.getPlayer(color).time
        while (active){
            timeLeft = gameInfo.getPlayer(color).time - (System.currentTimeMillis() - receivedTime)
            delay(1000)
        }
    }

    Box(modifier = Modifier.clip(RoundedCornerShape(Dimens.cornerBig))) {
        Box(modifier = Modifier.background(
            if(active) Colors.clockBackgroundActive else Colors.clockBackgroundDefault
        ).defaultMinSize(minWidth = 60.dp), contentAlignment = Alignment.Center){
            Text(
                if(gameInfo.configuration.infiniteTime) SpecialChars.INFINITE else TimeUtils.getTimeAsString(timeLeft),
                style = MaterialTheme.typography.h4,
                fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(10.dp)
            )
        }
    }
}

fun GameInfo.getPlayer(color: Color) =
    if(color == Color.WHITE) white else black