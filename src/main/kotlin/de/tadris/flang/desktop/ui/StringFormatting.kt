package de.tadris.flang.desktop.ui

import de.tadris.flang_lib.Color

val Color.label get() = string(when(this){
    Color.BLACK -> "game.color.black"
    Color.WHITE -> "game.color.white"
})