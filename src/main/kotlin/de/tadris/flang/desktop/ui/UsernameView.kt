package de.tadris.flang.desktop.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import de.tadris.flang.network_api.model.GamePlayerInfo
import de.tadris.flang.network_api.model.UserInfo
import de.tadris.flang_lib.Color
import kotlin.math.absoluteValue
import kotlin.math.roundToInt

@Composable
fun GamePlayerInfoView(playerInfo: GamePlayerInfo){
    Column(modifier = Modifier.padding(Dimens.paddingMedium)) {
        UsernameView(playerInfo)
        RatingView(playerInfo)
    }
}

@Composable
fun RatingView(playerInfo: GamePlayerInfo){
    Row {
        Text(playerInfo.getRatingText(), style = MaterialTheme.typography.body2)
        val ratingDiff = playerInfo.ratingDiff
        if(ratingDiff.absoluteValue > 0f){
            val sign = if(ratingDiff > 0) "+" else "-"
            val diffColor = if(ratingDiff > 0) Colors.ratingDiffPositive else Colors.ratingDiffNegative
            Text(sign + ratingDiff.absoluteValue.roundToInt().toString(), color = diffColor, style = MaterialTheme.typography.body2)
        }
    }
}

@Composable
fun UsernameView(userInfo: UserInfo){
    Row {
        if(userInfo.hasTitle()){
            Text("${userInfo.getDisplayedTitle()} ", color = userInfo.getTitleColor(), fontWeight = FontWeight.Bold, style = MaterialTheme.typography.body1)
        }
        Text(userInfo.username, style = MaterialTheme.typography.body1)
    }
}

fun UserInfo.getTitleColor(): androidx.compose.ui.graphics.Color {
    return if(isBot) Colors.botTitleColor else Colors.defaultTitleColor
}