package de.tadris.flang.desktop.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.text.font.FontStyle
import de.tadris.flang.network_api.model.GameConfiguration
import de.tadris.flang.network_api.model.GameInfo
import de.tadris.flang.network_api.model.GamePlayerInfo
import de.tadris.flang_lib.Board

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun AnalysisGame(openScreen: (screen: Screen) -> Unit, fmn: String, isFlippedAtStart: Boolean){
    val white = GamePlayerInfo("Player 1", 0f, -1L, 0f, false, "")
    val black = GamePlayerInfo("Player 2", 0f, -1L, 0f, false, "")

    val config = GameConfiguration(isRated = false, infiniteTime = true, time = -1L)
    val gameInfo = GameInfo(1, white, black, "", 0, true, config, 0L, 0, 0)

    var board by remember { mutableStateOf(Board.fromFMN(fmn)) }

    GameScreen(board, gameInfo, System.currentTimeMillis(), { newBoard, move ->
        board = newBoard.clone(true).executeOnBoard(move)
    }, null, creative = true, isFlippedAtStart = isFlippedAtStart, openScreen = openScreen, leftSide = {
        LeftSide(board){ board = it }
    })
}

@Composable
private fun LeftSide(board: Board, onBoardUpdate: (Board) -> Unit){
    var fmn by remember { mutableStateOf(board.getFMN2()) }
    var fbn by remember { mutableStateOf(board.getFBN2()) }

    fun applyFmn(){
        try {
            onBoardUpdate(Board.fromFMN(fmn))
        }catch (ignored: Exception){ }
    }
    fun applyFbn(){
        try {
            onBoardUpdate(Board.fromFBN2(fbn))
        }catch (ignored: Exception){ }
    }


    Card(shape = RoundedCornerShape(Dimens.cornerMedium), modifier = Modifier.padding(Dimens.paddingMedium)) {
        Column {
            Text("FMN", style = MaterialTheme.typography.subtitle2)
            TextField(fmn, {
                fmn = it
                applyFmn()
            }, singleLine = true, keyboardActions = KeyboardActions(onDone = { applyFmn() }), modifier = Modifier.fillMaxWidth())
            Text("FBM", style = MaterialTheme.typography.subtitle2)
            TextField(fbn, { }, readOnly = true, modifier = Modifier.fillMaxWidth())
        }
    }

    LaunchedEffect(board){
        fmn = board.getFMN2()
        fbn = board.getFBN2()
    }
}