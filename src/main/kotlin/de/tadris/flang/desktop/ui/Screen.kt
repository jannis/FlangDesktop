package de.tadris.flang.desktop.ui

import com.arkivanov.essenty.parcelable.Parcelable

sealed class Screen : Parcelable {

    object HomeScreen : Screen()

    object OfflineGame : Screen()

    class AnalysisGame(val fmn: String = "", val isSwappedAtStart: Boolean = false) : Screen()

    object FlangTv : Screen()

}