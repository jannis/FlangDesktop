package de.tadris.flang.desktop.ui

import de.tadris.flang.network_api.model.GameConfiguration
import de.tadris.flang.network_api.model.GameInfo
import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.TimeUtils
import de.tadris.flang_lib.action.Resign
import java.util.concurrent.TimeUnit

fun getGameStateDescription(info: GameInfo, gameBoard: Board): String {
    val winningColor = info.getWinningColor() ?: ( if(gameBoard.hasWon(Color.WHITE)) Color.WHITE else Color.BLACK )
    val reason = if(!info.running) info.getWinningReason() else {
        when {
            gameBoard.moveList.actions.last() is Resign -> GameInfo.WinReason.RESIGN
            gameBoard.findKing(winningColor)!!.location.y == winningColor.winningY -> GameInfo.WinReason.BASE
            else -> GameInfo.WinReason.FLANG
        }
    }

    return reason.getLabel(winningColor) +
            SpecialChars.SEPARATION_POINT +
            string("game.description.victorious").format(winningColor.label)
}

fun getGameInfoDescription(info: GameInfo): String {
    val mins = info.configuration.getMins()
    val mode = string(if(info.configuration.isRated) "game.mode.rated" else "game.mode.casual")
    val timeZone = TimeUtils.TimeControlZone.getZone(info.configuration.infiniteTime, info.configuration.time).displayName
    return mins + SpecialChars.SEPARATION_POINT + timeZone + SpecialChars.SEPARATION_POINT + mode
}

private fun GameConfiguration.getMins() = when {
    infiniteTime -> SpecialChars.INFINITE
    time == 30_000L -> "½ min"
    else -> "${time / TimeUnit.MINUTES.toMillis(1)} min"
}

private fun GameInfo.WinReason.getLabel(winningColor: Color) = when(this){
    GameInfo.WinReason.FLANG, GameInfo.WinReason.BASE -> string("game.win_reason.flang")
    GameInfo.WinReason.TIMEOUT -> string("game.win_reason.timeout").format(winningColor.getOpponent().label)
    GameInfo.WinReason.RESIGN -> string("game.win_reason.resign").format(winningColor.getOpponent().label)
    GameInfo.WinReason.UNDECIDED -> string("game.win_reason.draw")
}