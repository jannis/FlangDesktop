package de.tadris.flang.desktop.ui

import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.ui.graphics.Color

object Colors {

    val lightColors = lightColors(
        primary = Color(0xFF3F51B5),
        primaryVariant = Color(0xFF303F9F),
        secondary = Color(0xFF66BB6A),
        secondaryVariant = Color(0xFF388E3C),
    )

    val darkColors = darkColors(
        primary = Color(0xFF9FA8DA),
        primaryVariant = Color(0xFF303F9F),
        secondary = Color(0xFF66BB6A),
        secondaryVariant = Color(0xFF388E3C),
    )

    val selectedMove = Color(0x50007df3)

    val frozen = Color(0x6900C7B0)
    val fieldSelected = Color(0x8014551E)
    val fieldOption = Color(0x76009a00)

    val defaultTitleColor = Color(0xFF66BB6A)
    val botTitleColor = Color(0xFF3F51B5)

    val ratingDiffPositive = Color(0xff99cc00)
    val ratingDiffNegative = Color(0xffff4444)

    val clockBackgroundDefault = Color(0x80989898)
    val clockBackgroundActive = Color(0x8000BA1B)

}