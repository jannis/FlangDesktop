package de.tadris.flang.desktop.ui

object Images {

    private const val root = "images"
    const val logo = "$root/ic_launcher.png"
    const val whiteFlanger = "$root/wf.png"
    const val boardBackground = "$root/ic_brown.xml"
    const val analysis = "$root/ic_analysis.xml"
    const val back = "$root/ic_back.xml"
    const val book = "$root/ic_book.xml"
    const val chat = "$root/ic_chat.xml"
    const val computerAnalysis = "$root/ic_computer_analysis.xml"
    const val group = "$root/ic_group.xml"
    const val help = "$root/ic_help.xml"
    const val hint = "$root/ic_hint.xml"
    const val importExport = "$root/ic_import_export.xml"
    const val left = "$root/ic_left.xml"
    const val person = "$root/ic_person.xml"
    const val phone = "$root/ic_phone.xml"
    const val resign = "$root/ic_resign.xml"
    const val right = "$root/ic_right.xml"
    const val selected = "$root/ic_selected.xml"
    const val send = "$root/ic_send.xml"
    const val share = "$root/ic_share.xml"
    const val spectator = "$root/ic_spectator.xml"
    const val swap = "$root/ic_swap.xml"
    const val tv = "$root/ic_tv_live.xml"



}