package de.tadris.flang.desktop.ui

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.*
import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.input.pointer.PointerEventType
import androidx.compose.ui.input.pointer.onPointerEvent
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import de.tadris.flang.desktop.Log
import de.tadris.flang_lib.*
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.action.Move
import kotlin.math.min

@Composable
@Preview
private fun BoardPreview(){
    BoardView(Board(), BoardConfiguration(isClickable = true, animate = true, isFlipped = false, myColor = null, moveIsAllowed = true))
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun BoardView(board: Board, configuration: BoardConfiguration, requestMove: ((Move) -> Unit)? = null){
    var size by remember { mutableStateOf(0) }
    var selectedPiece by remember { mutableStateOf<Piece?>(null) }
    var pressed by remember { mutableStateOf(false) }
    var selectedPosition by remember { mutableStateOf(Offset(0f, 0f)) }

    LaunchedEffect(board){
        selectedPiece = null
        pressed = false
    }

    // List of possible targets for the selected piece
    val options = selectedPiece?.getPossibleMoves()?.map { it.target } ?: emptyList()

    fun unproject(offset: Offset): Location {
        val scale = size.toFloat() / Board.BOARD_SIZE
        val fieldX = (offset.x / scale).toInt()
        val fieldY = (offset.y / scale).toInt()
        return if(configuration.isFlipped) Location(board, Board.BOARD_SIZE - 1 - fieldX, fieldY)
        else Location(board, fieldX, Board.BOARD_SIZE - 1 - fieldY)
    }

    fun onPress(offset: Offset){
        if(!configuration.isClickable) return
        pressed = true
        selectedPosition = offset
        val location = unproject(offset)
        Log.d("MoveDetector", location.toString())
        if(!configuration.moveIsAllowed) return
        val piece = location.piece.value
        if(selectedPiece == null){
            if(piece != null && piece.color == location.board.atMove && !location.board.gameIsComplete()){
                if(configuration.myColor == null || (location.board.atMove == configuration.myColor && configuration.myColor == piece.color)){
                    Log.d("MoveDetector", "Selecting $piece")
                    selectedPiece = piece
                }
            }
        }else{
            val clickedOnSelected = selectedPiece!!.location == location
            if(!options.contains(location)){
                selectedPiece = null
                if(piece != null && !clickedOnSelected){
                    onPress(offset)
                }
            }
        }
    }

    fun onRelease(location: Location){
        if(!configuration.isClickable) return
        pressed = false
        if(options.contains(location)){
            requestMove?.invoke(Move(selectedPiece!!.location.piece.value!!, location))
            selectedPiece = null
        }
    }

    fun onMove(pos: Offset){
        if(pressed && selectedPiece != null){
            selectedPosition = pos
        }
    }

    Box(Modifier
        .onSizeChanged { size = min(it.width, it.height) }
        .aspectRatio(1f)
        .onPointerEvent(PointerEventType.Press){ onPress(it.changes.first().position) }
        .onPointerEvent(PointerEventType.Release){ onRelease(unproject(it.changes.first().position)) }
        .onPointerEvent(PointerEventType.Move){ onMove(it.changes.first().position) }
    ) {
        // background
        Image(painterResource(Images.boardBackground), "Board", modifier = Modifier.fillMaxSize())

        // pieces
        board.eachPiece(null){ piece ->
            val startPos = piece.getStartingPosition()
            key(startPos.x, startPos.y){
                PieceView(size, configuration, piece)
            }
        }

        // selected piece
        if(selectedPiece != null){
            ColoredView(size, configuration, selectedPiece!!.location, Colors.fieldSelected)
        }

        // possible moves
        options.forEach {
            ImagedFieldView(size, configuration, it, Images.selected)
        }

        if(selectedPiece != null && pressed){
            val fieldSize = size / Board.BOARD_SIZE
            val halfField = fieldSize / 2

            // drag target location
            val draggedLocation = unproject(selectedPosition)
            if(options.contains(draggedLocation)){
                ColoredView(size, configuration, draggedLocation, Colors.fieldOption)
            }

            // dragged piece
            Box(modifier = Modifier
                .fillMaxSize(1f / Board.BOARD_SIZE)
                .offset((selectedPosition.x - halfField).dp, (selectedPosition.y - halfField).dp)
            ) {
                PieceImage(selectedPiece!!, showFrozen = false)
            }
        }
    }
}

@Composable
fun PieceView(parentSize: Int, configuration: BoardConfiguration, piece: Piece){
    FieldView(parentSize, configuration, piece.location){
        PieceImage(piece, showFrozen = true)
    }
}

@Composable
fun PieceImage(piece: Piece, showFrozen: Boolean){
    Image(
        painterResource("images/" + getImageByPiece(piece) + ".png"),
        contentDescription = "Piece",
        modifier = Modifier
            .fillMaxSize()
            .background(if(piece.state == PieceState.FROZEN && showFrozen) Colors.frozen else androidx.compose.ui.graphics.Color.Transparent),
    )
}

@Composable
fun ImagedFieldView(parentSize: Int, configuration: BoardConfiguration, location: Location, icon: String){
    FieldView(parentSize, configuration, location){
        Image(
            painterResource(icon),
            contentDescription = "Piece",
            modifier = Modifier.fillMaxSize()
        )
    }
}

@Composable
fun ColoredView(parentSize: Int, configuration: BoardConfiguration, location: Location, color: androidx.compose.ui.graphics.Color){
    FieldView(parentSize, configuration, location){
        Box(Modifier.fillMaxSize().background(color))
    }
}

@Composable
fun FieldView(parentSize: Int, configuration: BoardConfiguration, location: Location, content: @Composable () -> Unit){
    val scale = parentSize.toFloat() / Board.BOARD_SIZE
    val animationSpec = if(configuration.animate) spring(visibilityThreshold = Dp.VisibilityThreshold) else snap()
    val displayedVector = configuration.project(location)
    val xOffset by animateDpAsState((scale * displayedVector.x).dp, animationSpec)
    val yOffset by animateDpAsState((scale * displayedVector.y).dp, animationSpec)

    Box(modifier = Modifier.fillMaxSize(1f / Board.BOARD_SIZE).offset(xOffset, yOffset)) {
        content()
    }
}

private fun getImageByPiece(piece: Piece): String {
    return when(piece.color){
        Color.BLACK -> when(piece.type){
            Type.PAWN -> "bp"
            Type.HORSE -> "bn"
            Type.ROOK -> "br"
            Type.UNI -> "bq"
            Type.FLANGER -> "bf"
            Type.KING -> "bk"
        }
        Color.WHITE -> when(piece.type){
            Type.PAWN -> "wp"
            Type.HORSE -> "wn"
            Type.ROOK -> "wr"
            Type.UNI -> "wq"
            Type.FLANGER -> "wf"
            Type.KING -> "wk"
        }
    }
}

data class BoardConfiguration(val isClickable: Boolean, val moveIsAllowed: Boolean, val animate: Boolean, val isFlipped: Boolean, val myColor: Color?){

    fun project(vector: Vector) =
        if(!isFlipped) Vector(vector.x, Board.BOARD_SIZE - 1 - vector.y)
        else Vector(Board.BOARD_SIZE - 1 - vector.x, vector.y)

}