package de.tadris.flang.desktop.data

import de.tadris.flang.network_api.FlangAPI

const val HOST = "home.tadris.de"
const val PORT = 9090
const val ROOT = "flang"
const val SSL_ENABLED = true
const val DEBUG = false

object DataProvider {

    private val api = FlangAPI(HOST, PORT, ROOT, SSL_ENABLED, DEBUG)

    fun accessOpenAPI() = api

}