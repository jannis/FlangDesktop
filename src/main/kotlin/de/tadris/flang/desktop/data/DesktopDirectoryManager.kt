package de.tadris.flang.desktop.data

import de.tadris.flang.desktop.isLinux

object DesktopDirectoryManager {

    private const val musicPath = ".flang"
    private val linuxPath = System.getProperty("user.home") + "/$musicPath"
    private val windowsPath = System.getenv("APPDATA") + "/$musicPath"

    private val rootPath = if (isLinux()) linuxPath else windowsPath

    fun getCachePath(): String {
        return "$rootPath/cache"
    }

    fun getDataPath(): String {
        return "$rootPath/data"
    }
}