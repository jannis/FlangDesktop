package de.tadris.flang.desktop

import androidx.compose.animation.Crossfade
import androidx.compose.material.MaterialTheme
import androidx.compose.foundation.layout.Row
import androidx.compose.runtime.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.input.key.Key
import androidx.compose.ui.input.key.KeyEventType
import androidx.compose.ui.input.key.key
import androidx.compose.ui.input.key.type
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import androidx.compose.ui.window.rememberWindowState
import com.arkivanov.decompose.Router
import com.arkivanov.decompose.extensions.compose.jetbrains.Children
import com.arkivanov.decompose.pop
import com.arkivanov.decompose.push
import de.tadris.flang.desktop.ui.*

private var escapeState by mutableStateOf(false)

fun main() = application {
    val darkTheme = false // TODO

    Window(
        onCloseRequest = ::exitApplication,
        title = "Flang",
        state = rememberWindowState(width = 800.dp, height = 500.dp),
        onKeyEvent = {
            if(it.type == KeyEventType.KeyDown){
                return@Window onKeyDown(it.key)
            }
            false
        }
    ) {
        MaterialTheme(
            colors = if(darkTheme) Colors.darkColors else Colors.lightColors,
        ) {
            MainView()
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class)
private fun onKeyDown(key: Key): Boolean = when(key){
    Key.Escape -> {
        escapeState = true
        true
    }
    else -> false
}

@Composable
fun MainView() {
    val router =
        rememberRouter<Screen>(
            initialConfiguration = { Screen.HomeScreen }
        )

    Row {
        Children(router.state){ child ->
            NavRail(child.configuration){ screen ->
                router.navigate { listOf(screen) }
            }
        }
        ContentView(router)
    }
}

@Composable
fun ContentView(router: Router<Screen, Any>) {

    LaunchedEffect(escapeState){
        if(escapeState){
            if(router.state.value.backStack.isNotEmpty()){
                router.pop()
            }
            escapeState = false
        }
    }

    val openScreen = { screen: Screen ->
        router.push(screen)
    }

    Children(
        routerState = router.state,
        animation = { state, childContent ->
            Crossfade(state) {
                childContent(it.activeChild)
            }
        }
    ) { screen ->
        when (val configuration = screen.configuration) {
            Screen.HomeScreen -> HomeScreen()
            Screen.OfflineGame -> OfflineGame(openScreen)
            is Screen.AnalysisGame -> AnalysisGame(openScreen, configuration.fmn, configuration.isSwappedAtStart)
            Screen.FlangTv -> TvGame(openScreen)
        }
    }
}