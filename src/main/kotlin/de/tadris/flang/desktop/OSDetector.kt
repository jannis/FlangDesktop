package de.tadris.flang.desktop

fun isLinux() = System.getProperty("os.name").lowercase() == "linux"