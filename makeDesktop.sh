#!/bin/bash

TARGET=$(grep 'desktop.target' local.properties)
TARGET=$(sed 's/desktop.target=//' <(echo "$TARGET"))
if [ -z "$TARGET" ]; then
  echo "Please create a file local.properties and specify the target path with:"
  echo "desktop.target=..."
  exit
fi

VERSION_STR=$(grep '^ *version' build.gradle.kts)
# shellcheck disable=SC2086
VERSION=$(sed 's/version = //' <(echo $VERSION_STR) | tr -d \" | xargs)

echo "Detected version $VERSION"

TARGET="$TARGET/$VERSION"

echo "Using target directory $TARGET"

echo "Cleaning"
gradle clean
echo "Building desktop package"
gradle package
echo "Building jar package"
gradle fatJar

mkdir "$TARGET"
cp build/libs/Flang*.jar "$TARGET/flang.jar"
cp build/compose/binaries/main/deb/*.deb "$TARGET/flang.deb"