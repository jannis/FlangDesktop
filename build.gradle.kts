import org.jetbrains.compose.compose
import org.jetbrains.compose.desktop.application.dsl.TargetFormat

plugins {
    kotlin("jvm")
    id("org.jetbrains.compose")
}

group = "de.tadris.flang"
version = "0.2.0"

repositories {
    google()
    mavenCentral()
    maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
}

dependencies {
    // Compose
    implementation(compose.desktop.currentOs)

    // Compose navigation
    implementation("com.arkivanov.decompose:decompose:0.3.1")
    implementation("com.arkivanov.decompose:extensions-compose-jetbrains:0.3.1")

    // Flang libraries
    implementation(files("./libs/network-api.jar"))
    implementation(files("./libs/flang-lib.jar"))

    // Dependencies of network-api
    implementation("com.squareup.okhttp3:okhttp:4.10.0")
    implementation("com.squareup.okhttp3:okhttp-urlconnection:4.10.0")
    implementation("com.google.code.gson:gson:2.9.0")
}

compose.desktop {
    application {
        mainClass = "de.tadris.flang.desktop.MainKt"
        nativeDistributions {
            targetFormats(TargetFormat.Msi, TargetFormat.Deb)
            packageName = "Flang Desktop"
            description = "Flang Desktop"
            copyright = "© 2022 Jannis Scheibe. All rights reserved."
            vendor = "Jannis Scheibe"
            packageVersion = version.toString()

            val iconsRoot = project.file("src/main/resources/images")
            windows {
                iconFile.set(File(iconsRoot, "ic_launcher.ico"))
                menuGroup = "Flang"
                upgradeUuid = "e08f6919-54a9-4992-a49a-471d674f2bd3"
            }
            linux {
                iconFile.set(File(iconsRoot, "ic_launcher.png"))
                packageName = "flang-desktop"
                debMaintainer = "flang@tadris.de"
                menuGroup = "Audio;AudioVideo;Player;Media"
                appCategory = "Audio;AudioVideo;Player;Media"
            }
        }
    }
}

tasks {
    register("fatJar", Jar::class.java){
        archiveClassifier.set("all")
        manifest {
            attributes["Main-Class"] = "de.tadris.flang.desktop.MainKt"
        }
        duplicatesStrategy = DuplicatesStrategy.EXCLUDE

        from(configurations
            .runtimeClasspath
            .get()
            .map { if (it.isDirectory) it else zipTree(it) })
        val sourcesMain = sourceSets.main.get()
        from(sourcesMain.output)
    }
}