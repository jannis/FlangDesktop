# Flang Desktop

[Flang](https://codeberg.org/jannis/Flang) is a board game similar to chess or shogi but differs in some special [rules](https://codeberg.org/jannis/Flang/wiki/Rules).

**Flang Desktop is currently unfinished. Please use the [Android App](https://codeberg.org/jannis/FlangAndroid) for full access.**

## Download

At the moment, you can download prebuilt versions from [here](https://home.tadris.de/nextcloud/index.php/s/o7rjK4ic2FPG2Nx).

## Client Features

- Play Flang: offline
- Flang TV: see other people playing Flang

Missing and planned features:

- Login and online gameplay
- Tutorial
- Global chat
- Opening book
- Computer analysis
- Search for players
- See player profiles
- See top players and statistics

## Forum and discussion

There is a [forum page](https://flang.tadris.de/)! You can ask questions there, help other players understanding the game, publish your new opening or strategies that you discovered.

Join the bridged chat on:

- Matrix: [#flang:matrix.org](https://matrix.to/#/#flang:matrix.org)
- Telegram: [@flanggame](https://t.me/flanggame)

## Development & Building

For development you can use IntelliJ IDEA. Just clone the project and open it in IDEA.
For running it in the IDE, start MainKt which has a main function.

To build an executable jar, use:

```bash
gradle fatJar
```

To build native packages (.msi on Windows, .deb on Ubuntu/Mint/...), you can use:

```bash
gradle package
```

There is also the script `makeDesktop.sh` that runs these commands and copies everything to a specified location.

## Notices

A HUGE thank you to [Lichess](https://lichess.org/) which is licensed under the GNU GPLv3 just like Flang.
Flang uses several assets such as piece images and game sounds from the Lichess repository in the Android client and I hope it's okay to use them.
If not, please mail me (address below).

## Donating

You want to donate? Here are some options:

<a href="https://liberapay.com/jannis/donate">
    <img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg">
</a>

* [Liberapay](https://liberapay.com/jannis/donate) (via PayPal)
* BTC: 3KDMLFrcDjeuXBM1CFpkHYBZLr23jF4JCw

You can also send a [mail](mailto:jannis@tadris.de) to get in touch with me.

## License

Copyright (C) 2022 Jannis Scheibe <jannis@tadris.de>

	Flang is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Flang is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Flang ist Freie Software: Sie können es unter den Bedingungen
    der GNU General Public License, wie von der Free Software Foundation,
    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
    veröffentlichten Version, weiter verteilen und/oder modifizieren.

    Flang wird in der Hoffnung bereitgestellt, dass es nützlich sein wird, jedoch
    OHNE JEDE GEWÄHR,; sogar ohne die implizite
    Gewähr der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
    Siehe die GNU General Public License für weitere Einzelheiten.

    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
    Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.